import os
import discord
import re
import json
 
import googleapiclient.discovery
import googleapiclient.errors
 
from datetime import datetime
 
from dotenv import load_dotenv
load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
 
CHANNELS = os.getenv('CHANNELS_DISCORD')
ROLE_AUTORISE = os.getenv('ROLE_AUTORISE')
 
YOUTUBE_API_KEY = os.getenv('YOUTUBE_API_KEY')
YT_CHANNELS = os.getenv('YT_CHANNELS')
 
## URL REGEX source : https://stackoverflow.com/questions/9760588/how-do-you-extract-a-url-from-a-string-using-python
url_regex = r'((?:(https?|s?ftp):\/\/)?(?:www\.)?((?:(?:[A-Z0-9][A-Z0-9-]{0,61}[A-Z0-9]\.)+)([A-Z]{2,6})|(?:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}))(?::(\d{1,5}))?(?:(\/\S+)*))'
 
#################################
## source : https://stackoverflow.com/questions/4705996/python-regex-convert-youtube-url-to-youtube-video
## VALIDATION URL ##
def youtube_url_validation(url):
  youtube_regex = (r'(https?://)?(www\.)?(youtube|youtu|youtube-nocookie)\.(com|be)/(watch\?v=|embed/|v/|.+\?v=)?([^&=%\?]{11})')
  youtube_regex_match = re.match(youtube_regex, url)
  return youtube_regex_match
 
## EXTRACTION VIDEO ID ##
def getVideoID(text_message):
  url = re.search("(?P<url>https?://[^\s]+)", text_message)
  if url:
    m = youtube_url_validation(url.group("url"))
    if m:
      video_ID = m.group(6)
      return video_ID
  return 0
 
#################################
## RECUPERATION CHANNEL ID ##
def getChannelID(video_ID):
  request = youtube.videos().list(part="snippet",id=video_ID)
  response = request.execute()
  return response["items"][0]["snippet"]["channelId"]
 
##################################
## VERIFICATION MESSAGE ENTRANT ##
def verifMessageASupprimer(m):
  find_urls_in_string = re.compile(url_regex, re.IGNORECASE)
  url = find_urls_in_string.search(m)
  if url is not None and url.group(0) is not None:
    videoID = getVideoID(m)
    if videoID != 0:
      channelID = getChannelID(videoID)
      if channelID in YT_CHANNELS:
        print('['+str(datetime.now().strftime('%d-%m-%Y, %H:%M:%S'))+'] /!\\ Suppression /!\\ # Video URL : '+str(url.group(0))+' # Channel ID : '+str(channelID))
        return True
  return False
 
 
 
#############################
## SCRIPT PRINCIPAL DU BOT ##
client = discord.Client()
 
@client.event
async def on_ready():
  print(f'** Bot {client.user} en ligne **\n')
 
@client.event
async def on_message(message):
  if message.author == client.user:
    return
  elif str(message.channel.id) in CHANNELS:
    if ROLE_AUTORISE not in str([role.id for role in message.author.roles]):
      if verifMessageASupprimer(message.content):
        await client.http.delete_message(message.channel.id, message.id)
 
api_service_name = "youtube"
api_version = "v3"
youtube = googleapiclient.discovery.build(api_service_name, api_version, developerKey = YOUTUBE_API_KEY)
client.run(TOKEN)
 
## FIN SCRIPT PRINCIPAL DU BOT ##
#################################