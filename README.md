Wendy Marvell
=================

Ce script permet de nettoyer des posts indésirables sur un canal donné dans un serveur discord.

Auteur
---------

- Espi0n

License
----------

Ce bout de script est disponsible sous le domaine public. https://creativecommons.org/publicdomain/zero/1.0/legalcode.fr

